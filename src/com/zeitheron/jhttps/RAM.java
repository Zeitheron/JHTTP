package com.zeitheron.jhttps;

import java.util.HashMap;
import java.util.Map;

import org.mozilla.javascript.NativeJavaObject;
import org.mozilla.javascript.Undefined;

import com.zeitheron.jhttps.io.Console;
import com.zeitheron.lib.json.serapi.Jsonable;

public class RAM
{
	private final Map<String, Object> MAP = new HashMap<>();
	
	public static Object unwrap(Object o)
	{
		if(o instanceof NativeJavaObject)
			return ((NativeJavaObject) o).unwrap();
		return o;
	}
	
	public RAM()
	{
		System.out.println("Linking memory map...");
		MAP.clear();
		System.out.println("Testing memory map...");
		if(put("console", Console.get()))
			System.out.println("assigned \"console\" to Console.get() : Instance");
	}
	
	public boolean rem(String key)
	{
		return put(key, null);
	}
	
	public boolean put(String key, Object value)
	{
		if(value == null)
		{
			boolean r = MAP.remove(key) != null;
			if(r)
				System.out.println("RAM: rem(\"" + Jsonable.formatInsideString(key) + "\", null)");
			return r;
		}
		
		boolean b = MAP.putIfAbsent(key, value) == null;
		if(b)
			System.out.println("RAM: put(\"" + Jsonable.formatInsideString(key) + "\", " + value.getClass().getName() + ")");
		return b;
	}
	
	public Object get(String key)
	{
		return unwrap(get(key, Undefined.instance));
	}
	
	public Object get(String key, Object def)
	{
		return MAP.getOrDefault(key, def);
	}
}