package com.zeitheron.jhttps;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.nio.file.Files;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.zeitheron.jhttps.io.FileMIME;
import com.zeitheron.jhttps.io.SimpleConfigs;
import com.zeitheron.jhttps.io.StringHelper;
import com.zeitheron.jhttps.js.JSStarter;
import com.zeitheron.jhttps.js.Natives;
import com.zeitheron.jhttps.js.Transposer;
import com.zeitheron.lib.io.IOUtils;

public class SimpleHTTP extends HttpServlet
{
	private static final long serialVersionUID = 1403209290613309243L;
	public static File dir;
	static SimpleConfigs path;
	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		if(!dir.isDirectory())
			dir.mkdirs();
		
		String path = req.getPathInfo().replace('/', File.separatorChar);
		if(path.startsWith(File.separatorChar + "*") && (req.getRemoteAddr().equals("0:0:0:0:0:0:0:1") || req.getRemoteAddr().equals("127.0.0.1")))
		{
			while(path.startsWith("/*/*"))
				path = path.substring(2);
			
			if(req.getPathInfo().equals("/*") || req.getPathInfo().equals("/*/"))
			{
				resp.setContentType("text/html");
				resp.getOutputStream().println("<script>window.location = \"/*/index.html\";</script>");
				return;
			}
			
			path = req.getPathInfo().replaceFirst("[*]", "adminpanel").replaceAll("[*]/", "");
			
			if(path.equals("/"))
			{
				path = "/index";
				String[] indexes = { "html", "ejs" };
				for(String type : indexes)
				{
					InputStream in = null;
					if((in = JServer.class.getResourceAsStream("/adminpanel/index." + type)) != null)
						path = "/index." + type;
					if(in != null)
						in.close();
				}
			}
			
			String fe = path.substring(path.lastIndexOf("/"));
			if(fe.contains("."))
				fe = fe.substring(fe.lastIndexOf('.') + 1);
			else
				fe = "";
			
			if(fe.equalsIgnoreCase("ejs") || fe.isEmpty())
			{
				JSStarter starter = JSStarter.executeInternalScript(path + "|" + req.getRemoteHost(), path, new Natives(resp));
				if(starter != null)
					Transposer.transpose(starter.callFunc("serve", req, resp), resp.getOutputStream());
			} else
			{
//				System.out.println("GET " + path);
				InputStream is = JServer.class.getResourceAsStream(path);
				if(is == null)
					return;
				resp.setContentType(URLConnection.guessContentTypeFromStream(is));
				is.close();
				
				is = JServer.class.getResourceAsStream(path);
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				IOUtils.pipeData(is, baos);
				is.close();
				resp.setContentLength(baos.size());
				try(ByteArrayInputStream fis = new ByteArrayInputStream(baos.toByteArray()))
				{
					ServletOutputStream sos = resp.getOutputStream();
					
					byte[] buf = new byte[1024];
					int r = 0;
					while((r = fis.read(buf)) > 0)
					{
						sos.write(buf, 0, r);
						sos.flush();
					}
				} catch(IOException ioe)
				{
				}
			}
			return;
		}
		
		File rf = new File(dir, path);
		
		if(rf.isDirectory()) /* request index */
		{
			File[] fs = rf.listFiles(name -> name.getName().startsWith("index"));
			if(fs.length > 0)
				rf = fs[0];
		}
		
		if(rf.isFile())
		{
			resp.setStatus(200);
			String fe = StringHelper.getFileExtension(rf);
			if(fe.equalsIgnoreCase("ejs") || fe.isEmpty())
			{
				JSStarter starter = JSStarter.executeScript(rf.getName() + "|" + req.getRemoteHost(), rf, new Natives(resp));
				Transposer.transpose(starter.callFunc("serve", req, resp), resp.getOutputStream());
			} else
			{
				resp.setContentType(FileMIME.getContentType(rf));
				resp.setContentLength((int) Files.size(rf.toPath()));
				try(FileInputStream fis = new FileInputStream(rf))
				{
					ServletOutputStream sos = resp.getOutputStream();
					
					byte[] buf = new byte[1024];
					int r = 0;
					while((r = fis.read(buf)) > 0)
					{
						sos.write(buf, 0, r);
						sos.flush();
					}
				} catch(IOException ioe)
				{
				}
			}
		} else
			resp.setStatus(404);
	}
}