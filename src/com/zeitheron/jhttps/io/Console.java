package com.zeitheron.jhttps.io;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import org.mozilla.javascript.Undefined;

import com.zeitheron.jhttps.ConsoleListener;
import com.zeitheron.jhttps.JServer;
import com.zeitheron.jhttps.Scheduler;
import com.zeitheron.lib.io.MultiOutputStream;

public class Console
{
	private static final Console INSTANCE = new Console();
	
	public static Console get()
	{
		return INSTANCE;
	}
	
	private final PipedOutputStream pos = new PipedOutputStream();
	private final PipedInputStream pis;
	
	public List<ConsoleListener> LISTENERS = new ArrayList<>();
	
	public int consoleLineLimit = 512;
	public final LinkedList<String> lines = new LinkedList<>();
	
	{
		PipedInputStream in;
		try
		{
			in = new PipedInputStream(pos, 32 * 1024);
		} catch(Throwable err)
		{
			in = null;
		}
		pis = in;
		System.setOut(new PrintStream(new MultiOutputStream(System.out, pos)));
		new Thread(this::thread1).start();
		new Thread(this::thread2).start();
	}

	public void println(String ln)
	{
		System.out.println(ln);
	}
	
	public void println(String ln, int color)
	{
		System.out.println("<font color = \"#" + Integer.toHexString(color) + "\">" + ln + "</font>");
	}
	
	public void println(String ln, String color)
	{
		System.out.println("<font color = \"" + color + "\">" + ln + "</font>");
	}
	
	public void processLine(String ln)
	{
		ln = ln.trim();
		System.out.println("> " + ln);
		
		if(ln.equalsIgnoreCase("stop") || ln.equalsIgnoreCase("exit") || ln.equalsIgnoreCase("shutdown"))
		{
			System.out.println("Shutting down!");
			Scheduler.schedule(() -> System.exit(0), 2000);
		} else if(ln.equalsIgnoreCase("version"))
			System.out.println("Current Version: " + JServer.VERSION);
		else
		{
			boolean proc = false;
			if(JServer.startupScript != null)
			{
				Object obj = JServer.startupScript.call("procCmd", ln);
				if(obj == null || Undefined.instance.equals(obj))
					; // Method not set - skip
				else if(obj != null)
					if(obj instanceof Boolean)
						proc = ((Boolean) obj).booleanValue();
					else
						System.err.println("Error in Startup Script: func procCmd return is not a boolean!");
			}
			if(!proc)
				System.out.println("Unknown command!");
		}
	}
	
	private void thread2()
	{
		Scanner sc = new Scanner(System.in);
		while(sc.hasNext())
		{
			String ln = sc.nextLine();
			processLine(ln);
		}
		sc.close();
	}
	
	private void thread1()
	{
		lines.addLast("");
		BufferedReader br = new BufferedReader(new InputStreamReader(pis));
		while(true)
			try
			{
				char ch = (char) br.read();
				
				if(ch == '\r')
					continue;
				if(ch == '\t') // Replace tabs
					ch = ' ';
				
				if(ch == '\n')
				{
					LISTENERS.forEach(l -> l.acceptLine(lines.getLast()));
					lines.addLast("");
				} else
				{
					lines.set(lines.size() - 1, lines.getLast() + ch);
				}
			} catch(Throwable err)
			{
				
			}
	}
	
	public void trimLog()
	{
		while(lines.size() > consoleLineLimit)
			lines.removeFirst();
	}
	
	@Override
	public String toString()
	{
		trimLog();
		return toString(0);
	}
	
	public String toString(int trimSize)
	{
		LinkedList<String> lines;
		
		if(trimSize > 0)
		{
			lines = new LinkedList<>(this.lines);
			while(lines.size() > trimSize)
				lines.removeFirst();
		} else
			lines = this.lines;
		
		StringBuilder sb = new StringBuilder();
		for(int i = lines.size() - 1; i >= 0; --i)
			sb.append(lines.get(i) + (i > 0 ? "\n" : ""));
		return sb.toString();
	}
}