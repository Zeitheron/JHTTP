package com.zeitheron.jhttps.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;

public class SimpleConfigs
{
	private final Map<String, String> PARAMS = new HashMap<>();
	private final Map<String, String> COMMENTS = new HashMap<>();
	private final iReadWritable file;
	
	public SimpleConfigs(iReadWritable file) throws IOException
	{
		this.file = file;
		load();
	}
	
	public void load() throws IOException
	{
		file.openConnection();
		InputStream in = file.input();
		if(in == null)
			return;
		Scanner sc = new Scanner(in);
		while(sc.hasNextLine())
		{
			String ln = sc.nextLine();
			if(ln.contains("="))
			{
				String[] kv = ln.split("=", 2);
				PARAMS.put(kv[0], kv[1]);
			}
		}
		sc.close();
		file.closeConnection();
	}
	
	public void save() throws IOException
	{
		file.openConnection();
		PrintWriter pw = new PrintWriter(file.output());
		for(String k : PARAMS.keySet())
		{
			String comment = COMMENTS.get(k);
			if(comment != null)
				for(String c : comment.split("\n"))
					pw.println("# " + c);
			pw.println(k + "=" + PARAMS.get(k));
			pw.println();
		}
		pw.close();
		file.closeConnection();
	}
	
	public void saveUnSafe()
	{
		try
		{
			save();
		} catch(IOException ioe)
		{
			throw new RuntimeException(ioe);
		}
	}
	
	public String getString(String key, String comment, Object defVal)
	{
		boolean save = false;
		if(!PARAMS.containsKey(key))
		{
			PARAMS.put(key, defVal + "");
			save = true;
		}
		save |= !Objects.equals(COMMENTS.get(key), comment);
		COMMENTS.put(key, comment);
		if(save)
			saveUnSafe();
		return PARAMS.get(key);
	}
}