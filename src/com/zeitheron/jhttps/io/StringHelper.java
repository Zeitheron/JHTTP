package com.zeitheron.jhttps.io;

import java.io.File;

public class StringHelper
{
	public static boolean isInt(String str)
	{
		return asInt(str) != null;
	}
	
	public static Integer asInt(String str)
	{
		try
		{
			return Integer.parseInt(str);
		} catch(Throwable err)
		{
		}
		return null;
	}
	
	public static String getFileExtension(File f)
	{
		String name = f.getName();
		if(!name.contains("."))
			return "";
		return name.substring(name.lastIndexOf('.') + 1);
	}
	
	public static File getLocationDir(File f)
	{
		String abs = f.getAbsolutePath();
		return new File(abs.substring(0, abs.lastIndexOf(File.separatorChar)));
	}
}