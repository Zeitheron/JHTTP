package com.zeitheron.jhttps.io;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class FileRW implements iReadWritable
{
	public final File file;
	
	private List<Closeable> closeables = new ArrayList<>();
	
	public FileRW(File file)
	{
		this.file = file;
	}
	
	@Override
	public void openConnection() throws IOException
	{
	}
	
	@Override
	public void closeConnection() throws IOException
	{
		while(!closeables.isEmpty())
			closeables.remove(0).close();
	}
	
	@Override
	public InputStream input() throws IOException
	{
		if(!file.isFile())
			return null;
		if(closeables.isEmpty())
		{
			InputStream in = new FileInputStream(file);
			closeables.add(in);
			return in;
		}
		return null;
	}
	
	@Override
	public OutputStream output() throws IOException
	{
		if(closeables.isEmpty())
		{
			OutputStream in = new FileOutputStream(file);
			closeables.add(in);
			return in;
		}
		return null;
	}
}