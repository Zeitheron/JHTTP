package com.zeitheron.jhttps.io;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class FileMIME
{
	public static final Map<String, String> MAPPING = new HashMap<>();
	
	static
	{
		assign("aac", "audio/aac");
		assign("abw", "application/x-abiword");
		assign("avi", "video/x-msvideo");
		assign("azw", "application/vnd.amazon.ebook");
		assign("bz", "application/x-bzip");
		assign("bz2", "application/x-bzip2");
		assign("csh", "application/x-csh");
		assign("css", "text/css");
		assign("csv", "text/csv");
		assign("doc", "application/msword");
		assign("eot", "application/vnd.ms-fontobject");
		assign("epub", "application/epub+zip");
		assign("gif", "image/gif");
		assign("htm", "text/html");
		assign("html", "text/html");
		assign("ico", "image/x-icon");
		assign("ics", "text/calendar");
		assign("jar", "application/java-archive");
		assign("jpeg", "image/jpeg");
		assign("jpg", "image/jpeg");
		assign("js", "application/javascript");
		assign("json", "application/json");
		assign("mid", "audio/midi");
		assign("midi", "audio/midi");
		assign("mpeg", "video/mpeg");
		assign("mpkg", "application/vnd.apple.installer+xml");
		assign("odp", "application/vnd.oasis.opendocument.presentation");
		assign("ods", "application/vnd.oasis.opendocument.spreadsheet");
		assign("oga", "audio/ogg");
		assign("ogv", "video/ogg");
		assign("ogx", "application/ogg");
		assign("otf", "font/otf");
		assign("png", "image/png");
		assign("pdf", "application/pdf");
		assign("ppt", "application/vnd.ms-powerpoint");
		assign("rar", "application/x-rar-compressed");
		assign("rtf", "application/rtf");
		assign("sh", "application/x-sh");
		assign("svg", "image/svg+xml");
		assign("swf", "application/x-shockwave-flash");
		assign("tar", "application/x-tar");
		assign("tif", "image/tiff");
		assign("tiff", "image/tiff");
		assign("ts", "video/vnd.dlna.mpeg-tts");
		assign("ttf", "font/ttf");
		assign("vsd", "application/vnd.visio");
		assign("wav", "audio/x-wav");
		assign("weba", "audio/webm");
		assign("webm", "video/webm");
		assign("webp", "image/webp");
		assign("woff", "font/woff");
		assign("woff2", "font/woff2");
		assign("xhtml", "application/xhtml+xml");
		assign("xls", "application/vnd.ms-excel");
		assign("xml", "application/xml");
		assign("xul", "application/vnd.mozilla.xul+xml");
		assign("zip", "application/zip");
		assign("7z", "application/x-7z-compressed");
	}
	
	public static void assign(String extension, String ct)
	{
		MAPPING.put(extension, ct);
	}
	
	public static String getContentType(File f)
	{
		for(String key : MAPPING.keySet())
			if(f.getName().endsWith("." + key))
				return MAPPING.get(key);
			
		String ct = "application/octet-stream";
		try
		{
			ct = f.toURI().toURL().openConnection().getContentType();
		} catch(IOException ioe)
		{
		}
		return ct;
	}
}