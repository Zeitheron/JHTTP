package com.zeitheron.jhttps.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public interface iReadWritable
{
	void openConnection() throws IOException;
	
	void closeConnection() throws IOException;
	
	InputStream input() throws IOException;
	
	OutputStream output() throws IOException;
}