package com.zeitheron.jhttps;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.Charset;

import org.mortbay.jetty.Server;
import org.mortbay.jetty.servlet.ServletHandler;
import org.xml.sax.SAXException;

import com.zeitheron.jhttps.io.Console;
import com.zeitheron.jhttps.io.FileRW;
import com.zeitheron.jhttps.io.SimpleConfigs;
import com.zeitheron.jhttps.io.StringHelper;
import com.zeitheron.jhttps.js.Natives;
import com.zeitheron.jhttps.js.StartupScript;
import com.zeitheron.lib.ZeithieLib;
import com.zeitheron.lib.weupnp.AttuneResult;
import com.zeitheron.lib.weupnp.EnumProtocol;
import com.zeitheron.lib.weupnp.WeUPnP;

public class JServer
{
	public static final String VERSION = "06Jun2018";
	
	public static File runDir;
	
	public static final RAM ramData = new RAM();
	public static StartupScript startupScript;
	
	public static void call(String func, Object... args)
	{
		if(startupScript != null)
			startupScript.call(func, args);
	}
	
	public static void main(String[] args)
	{
		Console.get();
		System.setErr(System.out);
		try
		{
			run();
		} catch(Throwable er)
		{
			er.printStackTrace();
		}
	}
	
	public static File getRunDir()
	{
		String path = findPathJar(null).replace(File.separatorChar, '/');
		path = path.substring(0, path.lastIndexOf('/')) + "/jhttp";
		return new File(path.replace('/', File.separatorChar));
	}
	
	private static void run() throws Throwable
	{
		System.out.println("Starting JHTTP (" + VERSION + ") Server by Zeitheron (https://github.com/Zeitheron)");
		System.out.println("Using ZeithieLib (" + ZeithieLib.getVersion() + ")!");
		System.out.println("Feel free to report any issues that may arise at https://github.com/Zeitheron/JHTTP/issues");
		
		runDir = getRunDir();
		if(!runDir.isDirectory())
			runDir.mkdirs();
		
		File file = new File(runDir, "configs.txt");
		SimpleConfigs cfgs = new SimpleConfigs(new FileRW(file));
		
		char fs = File.separatorChar;
		
		SimpleHTTP.dir = new File(cfgs.getString("http", "What directory should HTTP files be stored?", runDir.getAbsolutePath() + "/http/".replace('/', fs)));
		Natives.secureDir = new File(cfgs.getString("http_secure", "What directory should secure files be stored?", runDir.getAbsolutePath() + "/secure/".replace('/', fs)));
		
		File startup = new File(runDir, "startup.js");
		
		if(!startup.isFile())
			System.out.println("WARN: " + startup.getName() + " not found!");
		else
			startupScript = new StartupScript(startup);
		
		call("initConfig", cfgs);
		
		SimpleHTTP.dir.mkdirs();
		Natives.secureDir.mkdirs();
		
		int port = StringHelper.asInt(cfgs.getString("port", "What port should the server run?", 8080));
		String portforward = cfgs.getString("portforward", "Should we portforward? (yes / no)", "yes");
		if(portforward.equalsIgnoreCase("yes") || portforward.equalsIgnoreCase("true") || portforward.equalsIgnoreCase("+"))
			try
			{
				System.out.println("*** Portforwarding...");
				WeUPnP pnp = new WeUPnP();
				pnp.setup();
				pnp.discover();
				pnp.logFound();
				AttuneResult res = pnp.attune(EnumProtocol.TCP, port, port, "JHTTPServer");
				Runtime.getRuntime().addShutdownHook(new Thread(() ->
				{
					try
					{
						res.undo();
					} catch(IOException | SAXException e)
					{
						e.printStackTrace();
					}
				}));
				System.out.println("*** ...Complete!");
			} catch(Throwable err)
			{
			}
		
		System.out.println("Running on http://localhost:" + port);
		Server serv = new Server(port);
		ServletHandler handler = new ServletHandler();
		handler.addServletWithMapping(SimpleHTTP.class, "/*");
		serv.setHandler(handler);
		serv.start();
		Desktop.getDesktop().browse(new URL("http://localhost:" + port + "/*/index.html").toURI());
		serv.join();
	}
	
	/**
	 * If the provided class has been loaded from a jar file that is on the
	 * local file system, will find the absolute path to that jar file.
	 * 
	 * @param context
	 *            The jar file that contained the class file that represents
	 *            this class will be found. Specify {@code null} to let
	 *            {@code LiveInjector} find its own jar.
	 * @throws IllegalStateException
	 *             If the specified class was loaded from a directory or in some
	 *             other way (such as via HTTP, from a database, or some other
	 *             custom classloading device).
	 */
	public static String findPathJar(Class<?> context) throws IllegalStateException
	{
		if(context == null)
			context = JServer.class;
		String rawName = context.getName();
		String classFileName;
		{
			int idx = rawName.lastIndexOf('.');
			classFileName = (idx == -1 ? rawName : rawName.substring(idx + 1)) + ".class";
		}
		
		String uri = context.getResource(classFileName).toString();
		if(uri.startsWith("file:"))
		{
			String sub = new File(uri.substring(5)).getAbsolutePath();
			String[] path = System.getProperty("java.class.path").split(File.pathSeparator);
			for(String p : path)
				if(sub.startsWith(p))
					return p;
		}
		if(!uri.startsWith("jar:file:"))
		{
			int idx = uri.indexOf(':');
			String protocol = idx == -1 ? "(unknown)" : uri.substring(0, idx);
			throw new IllegalStateException("This class has been loaded remotely via the " + protocol + " protocol. Only loading from a jar on the local file system is supported.");
		}
		
		int idx = uri.indexOf('!');
		// As far as I know, the if statement below can't ever trigger, so it's
		// more of a sanity check thing.
		if(idx == -1)
			throw new IllegalStateException("You appear to have loaded this class from a local jar file, but I can't make sense of the URL!");
		
		try
		{
			String fileName = URLDecoder.decode(uri.substring("jar:file:".length(), idx), Charset.defaultCharset().name());
			return new File(fileName).getAbsolutePath();
		} catch(UnsupportedEncodingException e)
		{
			throw new InternalError("default charset doesn't exist. Your VM is borked.");
		}
	}
}