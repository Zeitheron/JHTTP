package com.zeitheron.jhttps;

public class Scheduler
{
	public static void schedule(Runnable func, long delay)
	{
		new Thread(() ->
		{
			try
			{
				Thread.sleep(delay);
			} catch(InterruptedException e)
			{
				e.printStackTrace();
			}
			func.run();
		}).start();
	}
}