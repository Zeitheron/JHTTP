package com.zeitheron.jhttps.js;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import javax.servlet.http.HttpServletResponse;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.Function;
import org.mozilla.javascript.NativeArray;
import org.mozilla.javascript.Scriptable;
import org.mozilla.javascript.ScriptableObject;
import org.mozilla.javascript.annotations.JSFunction;
import org.mozilla.javascript.annotations.JSStaticFunction;

import com.zeitheron.jhttps.JServer;
import com.zeitheron.jhttps.RAM;
import com.zeitheron.jhttps.Scheduler;
import com.zeitheron.jhttps.SimpleHTTP;
import com.zeitheron.jhttps.io.Console;
import com.zeitheron.jhttps.io.FileMIME;

@SuppressWarnings("serial")
public class Natives extends AbstractSuperBody
{
	public static File secureDir;
	public final HttpServletResponse response;
	
	public Natives(HttpServletResponse response)
	{
		this.response = response;
	}
	
	@JSFunction
	public String getContentType(Object f)
	{
		return FileMIME.getContentType((File) f);
	}
	
	@JSStaticFunction
	public static byte[] byteArr(NativeArray na)
	{
		byte[] bl = new byte[na.size()];
		for(int i = 0; i < bl.length; ++i)
			bl[i] = (byte) na.get(i);
		return bl;
	}
	
	@JSStaticFunction
	public static Object getConsole()
	{
		return Console.get();
	}
	
	@JSFunction
	public void println(Object ln)
	{
		try
		{
			response.getOutputStream().println(ln + "");
		} catch(IOException e)
		{
			throw new RuntimeException(e);
		}
	}
	
	@JSFunction
	public Object getSecureDir()
	{
		return secureDir;
	}
	
	@JSFunction
	public Object getHttpDir()
	{
		return SimpleHTTP.dir;
	}
	
	@JSFunction
	public void log(Object msg)
	{
		System.out.println(RAM.unwrap(msg));
	}
	
	@JSFunction
	public String readLn()
	{
		return ((Scanner) JServer.ramData.get("cin")).nextLine();
	}
	
	@JSFunction
	public Object getRAM()
	{
		return JServer.ramData;
	}
	
	@JSFunction
	public void schedule(int del, Object run)
	{
		if(run instanceof Runnable)
			Scheduler.schedule((Runnable) run, del);
		else if(run instanceof Function)
		{
			Scheduler.schedule(() ->
			{
				Function f = (Function) run;
				Scriptable scope = ScriptableObject.getTopLevelScope(starter.scope);
				Context.call(null, f, scope, starter.scope, new Object[0]);
			}, del);
		}
	}
}