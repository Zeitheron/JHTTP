package com.zeitheron.jhttps.js;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.Function;
import org.mozilla.javascript.Script;
import org.mozilla.javascript.ScriptableObject;
import org.mozilla.javascript.Undefined;

import com.zeitheron.jhttps.JServer;
import com.zeitheron.lib.utils.Joiner;

public class JSStarter
{
	public File executable;
	public ScriptableObject scope;
	public AbstractSuperBody body;
	
	public static JSStarter executeScript(String name, File executable, AbstractSuperBody nats, FJInj... injects) throws IOException
	{
		String text = "\n" + Joiner.NEW_LINE.join(Files.readAllLines(executable.toPath()));
		return JSStarter.executeScript(text, name, executable, nats, injects);
	}
	
	public static JSStarter executeInternalScript(String name, String path, AbstractSuperBody nats, FJInj... injects) throws IOException
	{
		InputStream in = JServer.class.getResourceAsStream(path);
		if(in == null)
			return null;
		Scanner sc = new Scanner(in);
		List<String> lines = new ArrayList<>();
		while(sc.hasNextLine())
			lines.add(sc.nextLine());
		sc.close();
		in.close();
		String text = "\n" + Joiner.NEW_LINE.join(lines);
		return JSStarter.executeScript(text, name, null, nats, injects);
	}
	
	public static JSStarter executeScript(String input, String name, File executable, AbstractSuperBody nats, FJInj... injects)
	{
		ScriptableObject scope;
		JSStarter js = new JSStarter();
		StringBuilder injectStr = new StringBuilder();
		for(FJInj i : injects)
			injectStr.append(i.toJSCode());
		input = injectStr.toString() + input;
		Context c = Context.enter();
		c.setLanguageVersion(Context.VERSION_1_8);
		Script scr = c.compileString(input, name, 0, (Object) null);
		js.scope = scope = c.initStandardObjects(nats, false);
		js.executable = executable;
		js.body = nats;
		nats.starter = js;
		scope.defineFunctionProperties(Natives.getAllBuiltinMethods(nats.getClass()), nats.getClass(), 2);
		scr.exec(c, scope);
		Context.exit();
		return js;
	}
	
	public Object callFunc(String name, Object... args)
	{
		if(!(ScriptableObject.getProperty(this.scope, name) instanceof Function))
			return Undefined.instance;
		return ScriptableObject.callMethod(this.scope, name, args);
	}
}