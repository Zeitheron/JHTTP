package com.zeitheron.jhttps.js;

import java.lang.reflect.Method;
import java.util.ArrayList;

import org.mozilla.javascript.ImporterTopLevel;
import org.mozilla.javascript.annotations.JSFunction;
import org.mozilla.javascript.annotations.JSStaticFunction;

@SuppressWarnings("serial")
public class AbstractSuperBody extends ImporterTopLevel
{
	public JSStarter starter;
	
	public static String[] getAllBuiltinMethods(Class<? extends AbstractSuperBody> c)
	{
		ArrayList<String> methods = new ArrayList<String>();
		Method[] arrmethod = c.getMethods();
		int n = arrmethod.length;
		int n2 = 0;
		while(n2 < n)
		{
			Method m = arrmethod[n2];
			if(m.getAnnotation(JSFunction.class) != null || m.getAnnotation(JSStaticFunction.class) != null)
				methods.add(m.getName());
			++n2;
		}
		return methods.toArray(new String[0]);
	}
}