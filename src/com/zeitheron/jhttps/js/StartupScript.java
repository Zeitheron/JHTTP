package com.zeitheron.jhttps.js;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.Function;
import org.mozilla.javascript.NativeArray;
import org.mozilla.javascript.Scriptable;
import org.mozilla.javascript.ScriptableObject;
import org.mozilla.javascript.annotations.JSFunction;
import org.mozilla.javascript.annotations.JSStaticFunction;

import com.zeitheron.jhttps.ConsoleListener;
import com.zeitheron.jhttps.JServer;
import com.zeitheron.jhttps.RAM;
import com.zeitheron.jhttps.Scheduler;
import com.zeitheron.jhttps.SimpleHTTP;
import com.zeitheron.jhttps.io.Console;
import com.zeitheron.jhttps.io.FileMIME;

@SuppressWarnings("serial")
public class StartupScript implements ConsoleListener
{
	public final JSStarter scope;
	
	public StartupScript(File script) throws IOException
	{
		if(script == null)
			scope = null;
		else
			scope = JSStarter.executeScript(script.getName(), script, new StartupBody());
		Console.get().LISTENERS.add(this);
	}
	
	public Object call(String func, Object... args)
	{
		try
		{
			return scope.callFunc(func, args);
		} catch(Throwable err)
		{
			System.err.println("Startup script threw an error:");
			err.printStackTrace();
		}
		return null;
	}
	
	public static class StartupBody extends AbstractSuperBody
	{
		
		@JSFunction
		public String getContentType(Object f)
		{
			return FileMIME.getContentType((File) f);
		}
		
		@JSStaticFunction
		public static byte[] byteArr(NativeArray na)
		{
			byte[] bl = new byte[na.size()];
			for(int i = 0; i < bl.length; ++i)
				bl[i] = (byte) na.get(i);
			return bl;
		}
		
		@JSStaticFunction
		public static Object getConsole()
		{
			return Console.get();
		}
		
		@JSFunction
		public Object getSecureDir()
		{
			return Natives.secureDir;
		}
		
		@JSFunction
		public Object getHttpDir()
		{
			return SimpleHTTP.dir;
		}
		
		@JSFunction
		public void log(Object msg)
		{
			System.out.println(RAM.unwrap(msg));
		}
		
		@JSFunction
		public String readLn()
		{
			return ((Scanner) JServer.ramData.get("cin")).nextLine();
		}
		
		@JSFunction
		public Object getRAM()
		{
			return JServer.ramData;
		}
		
		@JSFunction
		public void schedule(int del, Object run)
		{
			if(run instanceof Runnable)
				Scheduler.schedule((Runnable) run, del);
			else if(run instanceof Function)
			{
				Scheduler.schedule(() ->
				{
					Function f = (Function) run;
					Scriptable scope = ScriptableObject.getTopLevelScope(starter.scope);
					Context.call(null, f, scope, starter.scope, new Object[0]);
				}, del);
			}
		}
	}
	
	@Override
	public void acceptLine(String token)
	{
		call("console_acceptln", token);
	}
}