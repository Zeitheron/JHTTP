package com.zeitheron.jhttps.js;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;

import javax.servlet.ServletOutputStream;

import org.mozilla.javascript.NativeArray;
import org.mozilla.javascript.NativeJavaObject;
import org.mozilla.javascript.Undefined;

public class Transposer
{
	public static void transpose(Object obj, ServletOutputStream sos) throws IOException
	{
		/** Skip null/undefined objects */
		if(obj == null || obj instanceof Undefined)
			return;
		
		if(obj instanceof InputStream)
			try(InputStream is = (InputStream) obj)
			{
				byte[] buf = new byte[1024];
				int r = 0;
				while((r = is.read(buf)) > 0)
				{
					sos.write(buf, 0, r);
					sos.flush();
				}
				is.close();
			}
		else if(obj instanceof byte[])
			transpose(new ByteArrayInputStream((byte[]) obj), sos);
		else if(obj instanceof NativeArray)
			transpose(Natives.byteArr((NativeArray) obj), sos);
		else if(obj instanceof CharSequence)
			transpose((obj + "").getBytes(), sos);
		else if(obj instanceof URL)
			transpose(((URL) obj).openStream(), sos);
		else if(obj instanceof URI)
			transpose(((URI) obj).toURL(), sos);
		else if(obj instanceof File)
			transpose(((File) obj).toURI(), sos);
		else if(obj instanceof NativeJavaObject)
			transpose(((NativeJavaObject) obj).unwrap(), sos);
		else
			transpose(obj + "", sos);
	}
}