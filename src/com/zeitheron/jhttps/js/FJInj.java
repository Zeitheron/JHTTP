package com.zeitheron.jhttps.js;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;

public class FJInj
{
	public final String[] methods, fields;
	public final Class<?> cls;
	
	public FJInj(Class<?> cls)
	{
		this.cls = cls;
		this.methods = getAllBuiltinMethods(cls);
		this.fields = getAllBuiltinFields(cls);
	}
	
	public String toJSCode()
	{
		StringBuilder b = new StringBuilder();
		
		b.append("var " + cls.getSimpleName() + " = {");
		
		for(String m : methods)
			b.append(m + ": " + cls.getName() + "." + m).append(",");
		
		for(String m : fields)
			b.append(m + ": " + cls.getName() + "." + m).append(",");
		
		return b.append("}\n").toString();
	}
	
	public static String[] getAllBuiltinMethods(Class<?> cls)
	{
		ArrayList<String> methods = new ArrayList<String>();
		Method[] arrmethod = cls.getMethods();
		int n = arrmethod.length;
		int n2 = 0;
		while(n2 < n)
		{
			Method m = arrmethod[n2];
			if(Modifier.isStatic(m.getModifiers()) && Modifier.isPublic(m.getModifiers()))
				methods.add(m.getName());
			++n2;
		}
		return methods.toArray(new String[methods.size()]);
	}
	
	public static String[] getAllBuiltinFields(Class<?> cls)
	{
		ArrayList<String> methods = new ArrayList<String>();
		Field[] arrmethod = cls.getFields();
		int n = arrmethod.length;
		int n2 = 0;
		while(n2 < n)
		{
			Field m = arrmethod[n2];
			if(Modifier.isStatic(m.getModifiers()) && Modifier.isPublic(m.getModifiers()))
				methods.add(m.getName());
			++n2;
		}
		return methods.toArray(new String[methods.size()]);
	}
}