package com.zeitheron.jhttps;

public interface ConsoleListener
{
	void acceptLine(String ln);
}